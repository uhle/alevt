#ifndef EDLINE_H
#define EDLINE_H

#include "misc.h"
#include "xio.h"

#define EDLEN 128

struct edline
{
    struct xio_win *xw;
    u8 *prompt;
    void (*exit_func)();
    void *exit_data;
    void (*old_handler)();
    void *old_data;

    u8 buf[EDLEN];
    int plen;		// prompt len
    int len;		// current len (excl prompt)
    int offs;		// current scroll offset
};

struct edline *edline_new(struct xio_win *xw, u8 *prompt, u8 *preset,
						void *exit_func, void *data);
void edline_abort(struct edline *el);

#endif
