#include "vt.h"
#include "misc.h"

//#define VFILL ""
#define VFILL "     "

#define HELP_HEADER							\
"........\6AleVT Online Help System       ",				\
"  \22`p0`0    p `0pppp                    ",				\
"\4\35\22\177 \177j5`p  \177 j5 j5        \7   Version \34",		\
"\4\35\22\177,\177j5\177.! +t>! j5        \7"VFILL VERSION" \34",	\
"  \22# #\42!\42#   \42   \42!                     ",

#define FLOF_DATA							\
	1, { {0x100,ANY_SUB}, {0x200,ANY_SUB}, {0x300,ANY_SUB},		\
	     {0x400,ANY_SUB}, {0x0ff,ANY_SUB}, {0x100,ANY_SUB} }


struct vt_page help_pages[] =
{
    { 0x900, 0, -1, 0, 0, (1<<26)-1, {
#include "vt900.out"
    }, FLOF_DATA },

    { 0x901, 4, -1, 0, 0, (1<<26)-1, {
#include "vt901-04.out"
    }, FLOF_DATA },

    { 0x901, 3, -1, 0, 0, (1<<26)-1, {
#include "vt901-03.out"
    }, FLOF_DATA },

    { 0x901, 2, -1, 0, 0, (1<<26)-1, {
#include "vt901-02.out"
    }, FLOF_DATA },

    { 0x901, 1, -1, 0, 0, (1<<26)-1, {
#include "vt901-01.out"
    }, FLOF_DATA },

#if 0
    { 0x902, 0, -1, 0, 0, (1<<26)-1, {
#include "vt902.out"
    }, FLOF_DATA },
#endif

    { 0x903, 0, -1, 0, 0, (1<<26)-1, {
#include "vt903.out"
    }, FLOF_DATA },

    { 0x910, 0, -1, 0, 0, (1<<26)-1, {
#include "vt910.out"
    }, FLOF_DATA },

    { 0x911, 2, -1, 0, 0, (1<<26)-1, {
#include "vt911-02.out"
    }, FLOF_DATA },

    { 0x911, 1, -1, 0, 0, (1<<26)-1, {
#include "vt911-01.out"
    }, FLOF_DATA },

    { 0x912, 2, -1, 0, 0, (1<<26)-1, {
#include "vt912-02.out"
    }, FLOF_DATA },

    { 0x912, 1, -1, 0, 0, (1<<26)-1, {
#include "vt912-01.out"
    }, FLOF_DATA },

    { 0x913, 0, -1, 0, 0, (1<<26)-1, {
#include "vt913.out"
    }, FLOF_DATA },

    { 0x914, 2, -1, 0, 0, (1<<26)-1, {
#include "vt914-02.out"
    }, FLOF_DATA },

    { 0x914, 1, -1, 0, 0, (1<<26)-1, {
#include "vt914-01.out"
    }, FLOF_DATA },

    { 0x915, 0, -1, 0, 0, (1<<26)-1, {
#include "vt915.out"
    }, FLOF_DATA },

    { 0x999, 0, -1, 0, 0, (1<<26)-1, {
#include "vt999.out"
    }, FLOF_DATA },
};

const int nr_help_pages = NELEM(help_pages);
